---
title: Fate.Agency
type: docs
---
# Welcome to Fate.Agency!
{{< hint info>}}
"Never Ever Give Up Or In!" - The Phoenix
{{< /hint >}}

### The Main Missions For Both Worlds
---
```
 ✹ To  Restore  Order  to  The  Federation 
 ✹ To  Fix  All  of  the Worlds 
 ✹ To  Protect  All  Worlds  &  Races
```
### New Current Missions For On Earth
---
```
 ✹ To  Guide  Those  Who  Seek  The  Truth
 ✹ To  Prepare  All  We  Can
 ✹ To  Wake  Up 
```
### My Personal Mission
---
```
 ✹ To  Go  Home.
 ✹ To  Rest.
 ✹ To  Live.
```
### Overview
---

**Welcome to Fate.Agency my name is Justice Aldine. I am a 35 year old female from the USA. I have a long story to tell you that is non-fiction. There will be those whom do not believe this story. They might think that this is all an [ARG](https://en.wikipedia.org/wiki/Alternate_reality_game) though this is not.**

**We are going to do our best to help everyone understand what is going on. I just ask that everyone be kind and patient as I, myself, am having serious personal issues right now. I tend to have trouble explaining things well. You will find that I will not change my "story" for pointless reasons. I only change things if I am given different information.**

**If you have any questions please please come to our Discord. Our link is located to the left our site here at the very bottom.**

**We will be doing out best to help guide you all. I do not claim to have all the answers. I might know of something, but if I don't have full details it might not be shared. Though I will try to be as clear as I can when answering questions.**

**I hope you enjoy your stay here. We really hope that things can work out and that we can repair all the worlds.**

**~Justice Aldine**

### Important Information
---
Please, if you ever feel the need to harm yourself or others, please seek help immediately. This site in no way wishes harm on anyone from Earth. We are fighting for better worlds. Yes, I completely believe in what I have written on this site. In no way do you need to or have to believe one thing here. So please know that killing yourself would never get you home or to another planet. Killing yourself would not get you out of a simulation either if this was one. If this turns out to not be true, then at least it makes for a great story.

**National Suicide Prevention Lifeline** - Hours: *Available 24 hours. Languages: English, Spanish.* ***800-273-8255***
