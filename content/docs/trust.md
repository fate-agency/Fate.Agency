# TRUST
### The Scale
---
**There is a Trust Scale that we use. Those listed will need to have a trust level. It can be postive or negative. It will range like so.**
```
   5 ☆ | Complete Trust.
   4 ☆ | Is Highly Trusted.
   3 ☆ | We have some trust, but are leery of trusting.
   2 ☆ | We have little trust in this person, but still trust them.
   1 ☆ | We have some trust, but need to find out a lot more about this person.
   0 ☆ | We are unsure if we can trust this person or not at all.
  -1 ☆ | We have no trust for this person, but they could still come back from this rating.
  -2 ☆ | This person has shown us that we can not trust them.
  -3 ☆ | We can not trust this person with our lives.
  -4 ☆ | This person has stated that they are the enemy.
  -5 ☆ | There is no trust what so ever and they have no chance of ever regaining our trust.
```
