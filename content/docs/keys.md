# The Keys
## Overview
---
The Keys are the key players in the fight to save the worlds, restore order, and protect. Not all Keys have been found yet. Some are still gathering as we speak. You might be a Key and not even know it.

### The Tribe (`+2.00`)
The Tribe is part of the overall family of this battle. It is a large family and will stop at nothing to stay together and help each other. These people have been accepted into this family no matter if they are of the same blood or not.

- The Blessed
- [The Cat](/docs/keys/the-cat)
- [The Cub](/docs/keys/the-cub)
- [The Dragon](/docs/keys/the-dragon)
- [The Duck](/docs/keys/the-duck)
- [The Faith](/docs/keys/the-faith)
- [The Fighter](/docs/keys/the-fighter)
- [The Flower](/docs/keys/the-flower)
- [The Fox](/docs/keys/the-fox)
- [The Lion](/docs/keys/the-lion)
- [The Nugget](/docs/keys/the-nugget)
- [The Partner](/docs/keys/the-partner)
- [The Phoenix](/docs/keys/the-lioness)
- [The Rabbit](/docs/keys/the-rabbit)
- [~~The Rose~~](/docs/keys/the-rose)
- [The Repairer](/docs/keys/the-repairer)
- The Researcher
- [The Silent](/docs/keys/the-silent)
- [The Young](/docs/keys/the-young)

### The Allies (`+1.00`)
The Allies are those who are fighting with The Tribe to fix and restore order to the worlds. They are known to be helping in one way or another.

- [The Arrow](/docs/keys/the-arrow)
- [The Detective](/docs/keys/the-dectective)
- [The Doctor](/docs/keys/the-doctor)
- [The Helper](/docs/keys/the-helper)
- [The Mistress](/docs/keys/the-mistress)
- [The Mystery](/docs/keys/the-mystery)
- The Starr

### The Unknown (`+0.00`)
The Unknown are those who do not fit in any other listing. As such these people can be moved at any point if they are found to fit any of the other lists. To our knowledge these people are not necessary enemies.

- [The Children](/docs/keys/the-children)
- [The Cold](/docs/keys/the-cold)
- The Heaven
- [The Mother](/docs/keys/the-mothers)
- [The Sun](/docs/keys/the-sun)

### The Enemy (`-1.00`)
The Enemy is those who are trying to cause chaos and disorder. Their goals are not limited to, but can be: Taking control of the worlds by any means, Stealing teach to advance their chaos, controlling others by any means, etc.

These are the ones to watch out for as we know that they have shown their true colors over time. They might be able to be redeemed, but must still answer for their crimes.

- [The Bewitched](/docs/keys/the-bewitched)
- [The Smoker](/docs/keys/the-smoker)
- [The Witch](/docs/keys/the-witch)
