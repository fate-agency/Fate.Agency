# The Plan
{{< hint info>}}
"There is always a backup plan to the backup plan." - The Lion
{{< /hint >}}
### The Known Plan
---
It is the next step in the ultimate plan. Though I am unsure of the complete plan as things can always change. Not everything is set in stone sadly. There are some events that will repeat themselves on copies or iterations of other planets.

We are currently on Earth. This is the 42nd iteration of Alpha. Alpha was the first home to humans. We are currently in a fight to save the human race. Alpha might now be a melting pot of many many other races besides humans, but the ones there are fighting for our survival.

We have many different groups trying to help the human race and all the worlds we live on. Some iterations are gone, some are on the verge of being lost, and some have survived and progressed way past Earth. Though all of the iterations are at risk.

We are fighting to save all of us. As The Federation is not necessarily working towards the same goals. As they see us as ants and would rather us not be around as they do not see what we can offer them that would progress their goals.

The Federation normally though would help and jump in to make sure we all were protected and helped. Though what we know now is that there is a corruption within the group. We are not sure who is at hand for the corruption, but there are those working to find out. Not all of The Federation knows of this corruption and those who don't know are still trying to save us, though the corruption is harming the how long it is taking.

Right now The Lion and his team are working to gather up the people of Alpha. We are unsure how he will do so, but it is said that it might be in October that things should happen to where we are gathered together. Then we will finally be able to return home. We might all start to remember more of our lives from Alpha as time passes as we wait. There are others from other worlds as well and they might start to recall their lives from there as well.

I have been told that we will NOT wake up in our bodies on Alpha (If you are originally from there). Right now if we wished to wake up in our bodies there we would have to be ready to wake ourselves by ourselves. Though you can not force waking yourself up at this point. And killing yourselves will NOT wake you up there or anywhere. We will end up going home in our current bodies. Some will be brought with us as well. Some not from home, but ones who will be welcomed as if it has always been their home.

Once we are able to get home we can finally be able to work on restoring order to The Federation and help save all of our family. All of our people. All of the human race. Be Leery of The Federation right now. The Resistance is a part of us. It was started so that we could finish the mission to save our people and fix those who have a ruling thumb over the universes.

