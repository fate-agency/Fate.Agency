# The Tribe

### Who is The Tribe?
---
The Tribe is a group of people who are considered family. They might be blood related or not. There are those who are not blood related that have been accepted into the Tribe. Like those who might be close friends of the Tribe or those who have actually been adopted into the Tribe. Those that marry into the Tribe often are also accepted into it. The Tribe is immutable. They are unchanging. That does not mean that they cannot or will not change. That means the world around them might change, but they will not. Their past could change, but they would remember both the old and new past. The history of their world might change as well, but they would remember the past history as well as the new one that was made. [The Immutable](/docs/the-immutable) are important to changing the worlds for the best.
