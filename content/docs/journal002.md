# Journal Entry 
##### 3/31/21

### Feelings
---
For years I have felt like I was not home. I would be in my house with my family, but I have never actually been home. I've been in and out of different places, yet those never felt like home. I would end up crying alone, just bawling to myself, ugly crying if you must know. Well I would do this and mouth "I want to go home." Though I could not get one word out or make a sound. I have done this often. I would be asked where home was. I did not know. They would ask if it was Heaven. I told them no. It was hard to explain. I just knew I had a true home somewhere else and not here.

I just could not explain anything. I couldn't tell them I felt EAarth was not my home, never my home. I remember I was young when this started happening, maybe mid teens. It always hurt me in my soul. Not only that I always felt I was waiting on someone to find me. And that has affected me my whole life. I knew at a young age that when I found them I would know them. If not by looks by a soul connection.

With everything that is going on there are a ton of feelings that are going around and my soul is now attached to so much. I can not deny that things are happening and will continue to happen. Things will start to snowball. They are just going to go faster and faster. So I am right now just waiting for that day. It is not quite yet though I feel it approaching faster then it originally was. I can not guarantee that things will happen on any certain date, as things can always change. I just know that we are on the right path of fate. As long as this keeps up how it is going things should continue to go faster and we should be able to return home soon. Much sooner then stated originally which was by the end of 2024.

So I am not going to give up as I can feel that things are going to happen. If not in the way I have seen, then they will happen a different way. But it will lead down the same road I have seen. I am looking forward to that day. The day William falls. Once that happens things will continue to only get better. Better for both worlds. Better for all people.

So I will continue to push on even though my soul is missing parts of it. Just don't give up ever. WE will find our way home.