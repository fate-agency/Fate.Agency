# Journal Entry 
##### 4/1/21

### My Thoughts & How They've Been
---
My car broke down March 9th 2021. Today is April 1st 2021. I have been stuck at home almost all this time. Today is going on 23 days without a vehicle. I have had such a rough time both physically, mentally, and emotionally.

Let me just tell you how my day to day was before the car broke down. I would normally get up daily, inbetween 9am to 12pm. I'd get ready for work. I'd go pick up The Duck so we could work together. We'd work all day driving around delivering stuff to people.

The Duck and I would talk, listen to music, and make working fun. There were days we would take a break. Which is always good to do.

Sense the car has broke down I've been stuck at home with only my mother. Shes great, but she's not a best friend. I've lost so many people these past few years.

I do get attached to people. I admit that. My love is unconditional for people I love. I can't go into all the details right now, but its hard when you love someone and can't tell them. Trust me that sucks.

Well I don't often get to talk with people face to face or voice wise. Especially being stuck in the house.

So being alone here has been such a test. Physically I am hurting more because off the lack of working how I was. But its been mentally exhausting. My thoughts tell me how certain people hate me, don't want me, don't want to talk to me, and so on.

I am trying hard not to think this way. I've had so many anxiety and panic attacks these 23 days that its not funny. It has emotionally drained me too.

I know what I'm fighting for. I'm fighting for my family, my friends, my people; both here and more so on Alpha. I love them all. I'm sick of everyone hurting in one way or another. My heart and soul screams out to fix these things.

I am broken. Though I will not give up. I love two here and will fight for them. I must fight if I'm to be able to spend my future with them both. I hope they can understand and support me as I fix my broken self. I need them like trees need water, like humans need air.

This Lioness tries to stay strong. Tries to hold everything in, yet it comes out at times and ruins things. I have so much on my mind. I am fixing myself the best I can. Its not an overnight process.

I'm definitely sorry to anyone I've burdened sense this all started. I thank those who have supported me so much. But being left alone with your thoughts can somethings be bad, yet yes they could be good.

I'm so worried I'm going to fuck this stuff up. That in itself is setting me up to lose. I care so much for The Duck and The Cat. I know somehow everything will be okay. The Lion keeps trying to reinforce that in me. Trying to calm me down. Trying to help me not make the mistakes that will happen if I don't stay calm.

So my mind has worries, so many worries. I worry for those I love and care for. I want them to be happy. I want to please them. I want to spoil them. I just want to be there for them. I worry about them.

I'm worried about the people of The Underground City a lot. As we as The Lion and how things are progressing there. I worrying for The Nugget and The Fighter too. Auntie J misses them a shiy ton. I worry about my body being at The Facility, as that's where The Smoker hangs around.

I need to remember some advice I got from `$REDACTED`.

"Don't worry. Don't be afraid."

Its so hard for me not to worry. Its hard not to fear at times too. Who wouldn't worry and fear for their family and people?  I will try to find a balance between , so that the worry and fear doesn't control me, but ill control it.

I just hope others that I love will understand I am in no way trying to be a burden, and in no way trying to add to their stress and anxiety. I am so sorry if I am. Just please understand that if I'm messaging you or telling you certain things, that means I trust you. It also means in one way or another you help me by just being around me.

I will work on things as I don't want to push away the few I have. I am sorry for all my screw ups. If not for me this whole issue would not have happened and we would still be at home. Though I am grateful for things I've learned in my lives, especially this life.

I am broken, but healing. It takes time, and thats all we have these days. I'm now waiting on The Lion to get things going, because it is to be soon for the first deadline. I'm crossing my fingers we will all be ready by then.
