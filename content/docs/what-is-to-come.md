# What IS To Come

### ☆Part 1: Please Note
---
```
So this is a note to all that things are not all set in stone to happen.
As it is right now there is a plan. There are backup plans for when
something that is unplanned happens. As of right now the plans stated
here from the date submitted or updated are live still. Yet they can
change. Things can change easy. Trust me I personally wish they
wouldn’t.
```
### ☆Part 2: Will something actually happen from June 24<sup>th</sup>
---
```
2021 to July 8<sup>th</sup> 2021?

Let’s go over how I got these dates real quick. Just one day I say up
and it popped in my head. It was only “14 to 16 weeks" as I heard in my
head. I questioned why that many weeks. I am still unsure. Though when I
figured out exactly when 14 to 16 weeks would hit. Well it would hit on
June 24<sup>th</sup> to July 8<sup>th</sup> of 2021.

I am personally unsure of the actual timeline things will happen at this
point. I will be telling what I have been told as well as I can. So
let’s continue.

There are so many things that can happen within two weeks. Here are a
couple things that will/should happen that could effect what is to come.

1.  The Strawberry Full Moon falls on June 24<sup>th</sup>.

2.  The UFO report should either fall on the 24<sup>th</sup> or
25<sup>th</sup> of June. As that is when the 180 days are up. If it
does not come out then, then it will more then likely drop within
these two weeks.

Now here are a few points that could be happening that I’ve been told.

1.  The Federation (which I will go into who they are below) will reveal
themselves to:

    1.  Everyone on Earth

    2.  Key Players Only

2.  People will wake up to what’s actually going on.

3.  People from other worlds will start to remember their true lives that they have from their home worlds.

##### Please note not everyone on Earth are from another world, though a lot of people are.
```
### ☆Part 2.01: Who is the Federation?
---
```
How am I to truly explain the Federation as I call them. Others tend to
call them this as well. They are actually called the Galactic
Federation. They rain over all worlds with their rules. They just have
not been able to access Earth for various reasons. As of right now they
have plans to reveal themselves soon. Once this happens people will
finally know for sure that there is more life then here on Earth.

I suggest to everyone to not fear when they come. Do not start fighting
or start wars. We will all need to work together to better both this
world as well as Alpha and any others. We are all going to need to start
to accept each other and work together. There will be those who will
fight or start wars out of fear.
```
### ☆Part 2.02: Why everyone on Earth?
---
```
So as the Federation reveals themselves they could do so with everyone
on Earth. This is the most likely outcome that my sources believe will
happen. We are sure there will be a ton of accepting people. Yet they’re
going to be scared. The Federation is not coming to invade us. They are
coming to help us all.

They want to help everyone here so letting the masses know they exist at
one time will help. Yet they’re not here to invade. They’re here to
gather a few people as well as to free Earth’s people from this prison
world
```
### ☆Part 2.03 Why only key players if so?
---
```
The Federation is going to gather certain key players to be voices for
Earth and Alpha. They have a choice that they could just contact just
key players due to what’s happening. Though this is only a small chance
that only key players will know about them. Though yes key players will
be contacted by the Federation.

I am unsure how many key players will be contacted as a lot have been
scared away. So at this point there are very few. Those who will play a
part will end up running to the thing that scares all others.
```
### ☆Part 3: When is the Galactic Federation Coming?
---
```
So far what I know is that the Galactic Federation is supposed to come
between June 24<sup>th</sup> to July 8<sup>th</sup>. There is a high
chance they are coming and will show themselves to all of Earth. Our
governments know about them already. They know they can not stop them
from coming no matter how hard they try to.

Once they are here there will be an extremely important meeting that
will be for the fate of both Earth and Alpha. This meeting has many who
are standing up for both Earth and Alpha. The one to decide the fate of
Earth alone has been found and isn’t Justice (me). I am though here to
stand for both worlds now.

So this big meeting is coming and if it doesn’t happen by the end of the
8<sup>th</sup>, something indeed has happen to cause delays.
```
### Alpha 4: The Fate of Earth & Alpha?
---
```
There are many fighting for the fate of the Earth we live on at this
moment. There are so many things that will happen in the future. At this
point William and the people under his control are fighting to rule over
both Alpha and the Earth. Will believes if he takes over here he can
move everyone from Alpha here. If he’d do this none of the people from
Alpha will remember a thing while here. Our brains as humans in this
early age do not use as much brain power as in the future.

So if William gets his way, the Earth is just doomed. As the World
Eaters will then destroy Alpha. If Alpha is destroyed Earth and all of
Alpha’s other surviving iterations will be consumed. This will then make
the World Eaters overpowered by far and they will be able to consume our
universe. After that all other universes.

If William loses, which will happen as long as we don’t ever give up or
in, the Earth and Alpha both will be able to move forward. Earth will
advance in technology, medicine, and more. Alpha will also be able to
help advance Earth as well.

Though there can still be war(s) that outbreaks from fear and seeking
power. So be prepared. I am others are trying to prevent these things.

At this point this is the end of what I have to share in the writing for
the 3<sup>rd</sup> part.
```
