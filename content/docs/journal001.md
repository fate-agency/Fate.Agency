# Journal Entry 
##### 3/27/21

### Why Am I Fighting?
---
That is the question isn't it? Why am I fighting to get back home? Why am I not giving up or in when everything right now feels hopeless? I know this much I am fighting for my family. I am fighting to get back to my home. I will struggle and it will only increase from now to when I am able to return to Alpha.

I have been struggling for the past week with many different issues. Lack of a vehicle causes me to be stuck at my home here on Earth. I can not see my friends, I can not go out besides my yard and front porch. It has been a struggle as everything weighs heavy on my shoulders. I have had a three day panic attack which is just now getting under control. You would never understand why I have had this happen. Just know that William is playing his part well.

I have made it this far and even though I want to give up I won't ever give up. I will most definitely never ever give in. I believe that even if I did give up and in I would still suffer how I am. I won't do either, because I have much to do. A mission to do. And it does not matter if anyone believes me or not. It is only a matter of time until others start to see. It will trigger their fly or fight.

If they are scared they will run away. Fly. If they understand they will join us and fight for the best of Earth and Alpha. I just have had a hard time with everything weighing on my shoulders. I can not get ahead here on Earth. I know I am not the only one fighting for a better life for myself and others. This Fate is set for me, but other's fates can still be changed for the better.

I must continue to fight for everyone and everything. Especially my family both here on Earth and there on Alpha. I can not give up or in and must continue to fight. I know I will continue to be tested over and over again. I will come out on top and will make it home soon.
