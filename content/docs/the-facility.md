# Places
## Alpha > The Capitol

### Gemini Labs
---
There is a building within [The Capitol](/docs/the-capitol) that is extremely tall. It is the tallest one within the city there. It is Gemini Labs that is currently headed by William, [The Smoker](/docs/keys/the-smoker). They tend keep people there that are being held against their will. They are inserted into a matrix like system that actually allows your soul to enter a body on another planet. That planet is currently called [Earth](/docs/earth). Their body on [Alpha](/docs/alpha) is still alive and they are still connected to their body while living on Earth. they can be woken up back on Alpha at anytime. When coming to Earth you will forget your life there normally. 

Gemini Labs was originally founded by [The Lion](/docs/keys/the-lion) and [The Phoenix](/docs/keys/the-lioness).
### The Technology & Science
---
The technology that they use was originally created to help those who were dying or sick have a normal life. It would serve as an escape for those from the horrors of Alpha that has taken over there. Soon The Capitol started to resort to using this technology as a prison system to keep people under control. While being under in this state you could live multiple lives in a month. Well you could live multiple lives until recently. They recently synced up time with Earth. Earth's time was going much much faster then Alpha's time. On Alpha in one month you could live around 5.5 lives maybe more if they did not pull you out.

They could also change things with other technology that they have created and sometimes have even stolen from others like [The Underground City](/docs/the-underground-city) which has more advanced technology then The Capitol. [The Lion](/docs/keys/the-lion) and [The Phoenix](/docs/keys/the-lioness) had their own business that they ran that The Smoker would then steal in one way or another unless they hid it somewhere that he or the people from The Capitol would not find it on their yearly visits.

Gemini Labs is constantly working on new technology as well as anything scientific. They run tests on humans that would be considered illegal here on Earth. Things that can change a human for the better or for the worse. Both the science and technology is much much more advanced then here. It has jumped leaps and bounds in hundreds of years if not more. Most things you would never actually believe to be true as you would think them fantasy.
### The Medications
---
They also have medications that can affect you in so many different ways. Most medications have instant effects. There are ones that can cause pain all over or in only certain areas of your body. As well as ones that can cause your mood to immediately change for the better or for the worse. Those can cause you to go crazy permanently if given to much and if your body cannot handle the medications. Or they can cause manic affects that could last for an extremely long time. Either way they can cause life long last effects that are undesired by the person who has these medications.

There are medications that can let you stay up for days on end so that you can work constantly. Most do not enjoy taking these medications that allow them to stay up for days on end.
