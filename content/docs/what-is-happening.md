# What Is Happening
### ☆Part 1: Alpha's Iterations and The World Eaters
---
```
Alpha had many chances to change the world for the better. Though with
William taking things into his hands there was a risk that The World
Eaters or worse would destroy Alpha and in turn destroy all of the
universe. Soon Tom and the others had found a way to help save Alpha.
And these worlds would be new iterations of Alpha.

Some would survive and thrive while others would vanish. William had
decided how best to keep Jessica, Jennifer, The Cat, and Ivy The Fox in
line then to place them on these new iterations of Alpha. All four would
live multiple lives on each iteration. And continue to be trapped in
them living new lives over and over again.

None of these lives proved to wake these ladies up at all. Not until
iteration 42, Earth. It would be Jessica's 6th lifetime when Tom would
try to completely guide her to wake her up. In this lifetime she would
go by Justice. A name that she would go by also on Alpha.

During her sixth life she would be contacted at a young age by Tom. She
would remember him as a boy, but not recall exactly what they would talk
about. Tom and the others would try to wake her so that she would
remember what her life was and who she was. It would not happen until
2020 when she would then wake to an interesting memory dream.
```
### ☆Part 1.01: What Are World Eaters?
---
```
The World Eaters are those who will take and drain the power of worlds and beings of any type. They take and live on the hopes and dreams of what could/should have been. If they gain Alpha they gain all the surviving iterations of Alpha. They will gain every being on these iterations as well. If they gain access to Alpha alone they will become over powered to where they'll have enough power to destroy the whole universe and beyond. 

#### Please note that at this point I know very little about World Eaters myself. As I learn more I will add the information.
```
### ☆Part 2: What Are Memory Dreams?
---
```
Memory dreams are phrased this way as they are memories that you
remember when dreaming. As this happens you will remember living certain
lives if you have past lives. Some might be mixed with other different
things that don't make sense. As these dreams happen and include such
things as memories, you might wonder how you tell a memory and just a
dream apart. Honestly you won't know until you have one. You will be
able to know that it is a memory more then likely. Sometimes you might
not be able to.

So when I refer to a memory dream, I am referring to a memory I have had
while asleep. There are others who've had these as well.
```
### ☆Part 3: I Want To Go Home
---
```
I ask why me now. Why? I always felt I was never home. That this Earth
is not my home. It is not home.

I'd ugly cry throughout my life screaming silently into the sky with my
voiceless voice

"I WANT TO GO HOME!! I NEED TO GO HOME!!!"

"This isn't home. Home isn't heaven. Home isn't hell... what is
home? Its not here on Earth."

Then back to

"I WANT TO GO HOME!! I NEED TO GO HOME!!!"

Its a repeating cycle.

I didn't understand. It would not come to light until I was 33 years
old in 2019.
```
### ☆Part 4: A Fight, A Surgery, A Broken Soul
---
```
Side Note: The dates I include are as close as I remember pre-surgery.

Back in 2015 I started my fight to get a much needed surgery. A surgery
to help me lose weight.

But before I get into that around 2014 I start to have a sense of
urgency that I need to get ready. Something was coming. I needed to be
in as good of form as I can be.

I knew I was needed. I had to rush as much as I could. I was confused as
I was being told I would be needed. Needed to lead. Be a leader of a
resistance. I was so confused.

I started pushing my doctor for this surgery. He was set against it. It
took over a year of pushing for him to refer me to the hospital. It
shouldn't have taken that long. It was as if something didn't want me
to proceed.

At this time I also had a fog in my brain. Horrible migraines. Something
wasn't right. Something was doing this. Something haunted me keeping me
from remembering things and being able to literally think during the
time from 2014 to April 2018.

I started my journey finally. But had to go 7 months to classes which
lasted 8 months as I somehow put on almost 20lbs in a month the 7th
month. And the next it was gone plus more. That was not normal. Finally
my surgery was set to be May 21st 2018.

Though the house we were in was horrible. We needed out. I meet a guy
online. Started dating him. And at the time I had 6 dogs. I wanted to
move back to the last town, but could only have 4 dogs.

Only about a month before we would find a new home I lost my beloved dog
Tilly. But what was worse only 3 days later I had to help my dogs
Strider pass. Well he was my soul dog. My soul mate in a dog. It was
weird. I had him 14.5 years. He lived 15.5. He should NOT have lived
past 12 years. But I lost both beloved dogs in 3 days.

So then it jumps to the guy I was dating wanted to move in. So I called
the old trailer court. Well they had a decent trailer for only $500. So
we grabbed it. We could even make that in 6 payments with our rent.

We moved in in April of 2018. My boyfriend was as asshole. But if not
for him I'd not of even looked into this home. It was Fate at work.

So only a month later I had my surgery on May 21st 2018. I was scared of
course and almost called it off. But no hell no! I'm needed soon. But
right after the surgery I wake up and feel my soul break. I was broken.
Horribly. I was in horrible pain.

I'd go home with pain medication. Oxy. It helped my pain a ton. But I
lost interest in EVERYTHING!!! Even Minecraft. It didn't help that the
boyfriend was not supportive.

Well I turned to YouTube, Oxy, sleeping, and eating. I couldn't even
exercise. The only thing to help pull me through was the YouTubers
Markiplier and Dad. Others helped too.

So I sat there broken for 18 months exactly. So on October 21st 2019 I
felt my soul heal. Something, or someone was able to fix me. I now
believe it was those fighting for me all along.
```
