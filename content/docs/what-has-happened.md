# What Has Happened
### ☆Part 1: Introduction
---
```
Many of us here on Earth believe that we are the only intelligent beings in our universe. Those who believe that are sadly mistaken. There are those who do believe there is life on many other worlds. I am here to write and tell you the story of what has happened, what is happening, and what is going to happen. We must all try to be as prepared as we can for the things to come.
	
I am here to encourage you to read though this whole story as this is not fiction. This is actually non-fiction. It is up to you to believe this or not. We all have that right to judge for ourselves what is actually true. I am not here to lie to you as this is an important mission I am on. You might be questioning who I am right now. Well I am Justice. You can call me what you want, but I am here to help fix the worlds and prepare others for what is to come.
	
I will tell you the story I have been told and also remember from memories I have had from my other life on a planet called Alpha. I will write in parts and invite you to read them as I upload them. There is a lot of information that I must share in a short amount of time.

Some of you might have felt like something big is going to happen. I have felts this for the past ten years at least. I will try to guide you as I write and explain all I can. If you have any questions please make sure to come and join us on our Discord so that we can help you understand. I saw we as there are more that are involved then just I. Now let me start from the beginning.
```
### ☆Part 2: In the beginning
---
```
This is going to be hard to explain as there are so many beings in our universe. Though there are more universes than just our universe. Let me start off with the life of two sisters. They are beautiful twins flying through the sky located in the universe where Alpha the first iteration resides. This world is older than Earth, though at this point Earth is yet to be born.

These two twins are not on bit human. They fly through the sky circling each other as they are star beings. They are actual stars that are living a loving carefree life together. These twin stars love each other as sisters do. They are bounded and never want to leave each other alone. They are both caring and sweet.

One day as they are swirling around each other carefree they sense danger. They see a darkness coming after them. They knew if they were captured by this being they would be forever forgotten. They were scared as this was a world eater trying to destroy their lives. They did not want to be wiped out. They decided to run away and find a hiding space.
	
The humans had named these two stars Castor and Pollux. Castor cared so much for Pollux that she had to seek out a solution to their problems. Castor had the idea to find a place on Alpha. Castor found a way for both of them to survive. They could not be apart. Castor took Pollux with her finding a woman who was to have children. This woman was to have twins.
	
Castor and Pullux were ready to live their new lives. Though it would not work out how both of these twin stars wished. Yes there were born twins to this woman. This woman named her eldest daughter Jessica and the other Jennifer. Though Jennifer was not either Castor or Pollux. Jennifer was a different soul. Now Jessica was different. Jessica was Castor. Though Jessica also housed Pollux in her. Both star sisters had been born in one body.
	
So Castor now was Jessica. This was her first world bound life. This body was Castor’s yet she was happy to share her life with Pollux. She loved her sister more than life itself. She also loved her other twin Jennifer.
```
### ☆Part 3: Split Apart
---
```
Both Jessica and Jennifer were still very young as their mother and father watched them. Their father was a great king on Alpha and the mother was of course the Queen. They saw potential in Jessica as she was the eldest and saw the Jennifer was not up to what they wanted at the time. Soon they found that Jessica housed two star being souls. They decided that she would do well being trained to fight in the war. They decided to send Jennifer away to another family to be raised. It was very early in the two girl’s lives that they would soon forget about the other.
```
### ☆Part 4: The War Part I
---
```
At a very young age Jessica was sent into training for the King's army. This king was her father yet was cruel. He and his wife only cared about themselves and ruling over everyone and everything on Alpha.The king had decided he would start a war across all of Alpha so that he would be the only one to rule the whole world.
	
During the early years of Jessica's life she would struggle with being trained in the army as well as the testing that the king had done to the young people of Alpha. The king would have kids taken from their parents either by paying a small fee for them or by force if the parents didn't agree.
	
At this time the king had many scientists working for him to help create super solders. This testing would help create people who could do amazing things that have yet to happen on Earth. Jessica was no accept ion to being tested on. Even the sister she had completely forgotten about was also tested on. The king loved to run tests on twins.
	
Jessica had a kind soul and heart, so did her star twin who still resided within her. Jessica would fight the orders she was given. As she was not one to want to kill those who she deemed innocent. She, herself did not like to even kill those who were not innocent. She had many powers she was given including being able to heal others. She would tend to heal both allies and enemies alike.
	
One time when she was a teenager she had rebelled far to many times. She found herself inserted into a pod that stood upright. A black goo surrounded her body. It was extremely painful. She wanted to cry out in pain and fight it. Though if she had it would drain her body of life and turn her into a lifeless zombie. She stood there in the the pod in severe pain.
	
As she stood there she saw from across the way a young solider standing there. He looked on seeing how strong she was. He was a leader in the army, a Major. He watched on and seeing how she took the pain and knew why she had been placed in there, feel for her. He at that moment decided to take her under his wing and to make sure that she would be able to survive. As if she kept failing at her missions she would be killed. Even being the king's daughter she would be killed, for the king sure as hell did not care for her. All the king wanted was to rule the whole of Alpha and he would do so any way that he could.
	
Soon this Major had requested that Jessica be in his group. She knew she was safe with him even though he acted harsh towards everyone else. When alone he was different. He wanted to protect her and they did not understand why they seemed to want to be together. Yet they were drawn to each other like soul mates.
	
Jessica's hero's name was Thomas. Both of them clicked together and were happy when they were around each other. Though it was forbidden to be in a relationship in the king's army. Especially sense the king owned these soldiers. Tom's family had been given the option to either give the king Tom or the king would take both Tom and his twin brother Nick. His family settled to allow the king to take only Tom.
	
Jessica was glad to have someone who could help her and help her not to have to kill anyone. It broke her heart each and every time someone had to die. There were times when the king had them going after families to capture the children so that the king would have even more soldiers. If Jessica could not handle it, Tom would do her part of the job. It did get to where here star twin would take over if Jessica could not handle a job and had no choice but to kill or be killed.
```
### ☆Part 5: The War Part II
---
```
Tom and Jessica were both extremely close in age. She was the first person that he ever truly wanted to protect. Both feel in love and fit together extremely well. The testing continued on them as well as all the other young soldiers. They started to fear that they would be found out and killed for being together. They had finally decided to try to find a way out of the army by running away.
	
Though now that they decided this they had a problem. They still had the trackers that had been placed in them. If push came to shove they could even be killed instantly if found to be running away. Years were passing as they planed their escape. They knew of a doctor who could remove said tackers. They had made plans to meet up with Doc. She was able to safely remove these trackers without causing damage to Jessica or Tom.
	
They were put in unspeakable spots that even they would not dare remove them themselves. Once these were removed they could leave and run.
```
### ☆Part 6: Intermission
---
```
So there is not much information between them running to when Jessica was to take the place of the king. It is a blank at the moment. We just know that both the king and queen had died of "natural" causes. Both Jessica and Tom went into the science field after taking over to help guide people.
	
They both founded Gemini Labs...
```
### ☆Part 7: Family
---
```
By this time Jessica and Tom had been hanging around Nick and they would find Jessica's twin sister by accident. Nick really really liked Jessica, but she loved Tom over everyone. Though when the three had found Jennifer, Nick would fall for her. He thought of her as a goddess. Soon all four would have a double wedding and live their lives in the same home. Jessica married Tom and Jennifer married Nick.
	
Soon Jessica and Tom would found Gemini Labs a scientific medical lab that focused on medicine and gynecology first and foremost.
```
### ☆Part 8: Gemini Labs
---
```
For years Tom and Jessica would run Gemini Labs. Both Jennifer and Nick would help work in the labs. Soon Tom and Jessica would have a young man name William working along side them. They took him under their wing. He was a very bright man. He was their age too. They considered him family and had accepted him into the family that they loved.
	
They focused on the medicine a lot and were working on helping people conceive children no matter who they were. The testing that the past king had had done to the people of Alpha had caused issues in them having children. Part of it was that the tests made it to where people would live even longer then they should have.
	
Tom and Jessica at this time were quite old. As well as most of the people from their birth time. They were much much older then you would ever guess. They were in their 900s. Jessica and Tom had wanted to test things so they could see if they could conceive children. They ended up being successful. Soon they would have their first son Markus.
	
Unbeknownst to Tom and Jessica, William had continued a project that they had headed. They had been also working on using a form of time travel to travel into the past. Somehow William had went back in time and changed the past of Alpha just enough to cause it to where now their was no longer a Queen in charge but a president.
	
Jessica's family or those who were accepted into the family had become immutable. Things could change around them, yet they would remember it how it use to be. This being the case people were now confused why there was a Queen lurking around and why she was a queen. It did not make sense to the people who were mutable why there was a president and queen, though they made excuses. Most didn't care that Jessica was a queen at this point.
	
William going behind the couples back and causing there to be a change in leadership, caused the president to hand over Gemini Labs to William. William had threatened their son Markus as both Jessica and Tom refused to step down at first. But in the end to protect their son they decided to go on the run.
```
### ☆Part 9: On the Run
---
```
Knowing now that William had not been who they thought he was the small family took off. Jessica and Tom took all the work they could that was for the biological creation of the making of children and the medications.
	
They took their research and Markus far far away in a secluded area. They had went to a cabin that they knew was not known by William. They were finally safe and would try to live their life to the fullest. Well it would not last. By the time Markus was four years old that time would come that the peace would end. A lady that was sent by William to find the family had found them.
	
Jessica laid in a chair holding her side. The lady who came to find them had attacked her and wounded her. Jessica was bleeding and having trouble getting up to warn Tom and to protect her son. This lady had grabbed the biological data that Jessica had been working on.
	
Jessica watched helpless as this lady set the cabin on fire. Jessica tried her hardest to heal herself. Finally once she was able to she went to rush into the cabin. Tom had already rushed in. He was trying to save the containers of pills. It was important to save those as medicine was limited on Alpha at the time.
	
Tom ran out with them and had caught on fire. Jessica ran to his side to put the fire out. They had saved the medicine, yet you might be wondering where Markus was. Markus was no longer in the cabin. He had been taken by William's lackey. This would not be the first time that Markus would be kidnapped and used as bait or to control the couple.
```
### ☆Part 10: Back Home
---	
```
Both parents were heart broken. They decided to head back to the city. Soon they started the Underground City that would become the most technological advanced place on Alpha. Jessica found she was pregnant again. She had conceived when at the cabin. Tom and her were happy even though their eldest son was being held hostage.
	
Soon they were able to get him back for a time. They had to conform to the "president's" will. They would advance science still while at the Underground City while leading the city. Jessica kept her title as Queen, but mainly known as the Queen of the city.
	
Nine months after Markus was kidnapped Johnathan was born. By this time Markus was five and back home. Yet Jessica and Tom knew that they had to do something even with the fear of William taking Markus again. They knew that William would do anything to control them.
```
### ☆Part 11: Jumping Ahead
---	
```
It is now many many years later and Jessica and Tom now have three kids. Markus, 29 years old, Johnathan, 25 years old and Crystal 16 years old. They also had their adopted daughter Ivy. They lived their lives the best they could within the Underground City. Though throughout the years William would kidnap both Markus and Johnathan many many times.
	
Jessica and Tom would protect their daughter and train her well so that she would never be captured. Every time that Markus or Johnathan would be captured to be used to blackmail the couple they would work hard to get them back home. Though one day Johnathan and Tom got into an fight and decided to join a man named X. X was from another faction on Alpha.
	
There was so much that had happened over the years on Alpha, but we must sadly make these jumps ahead to tell the more important parts. Not all can be written at this point.
```
### ☆Part 12: William and Pollux and Jessica
---	
```
Pollux had felt like a burden to both Tom and Jessica. She knew she was loved by her sister. She could take over Jessica's body when need be. Pollux was curious about William and at one time fallen for him. She feel in love with him and trusted him. She never thought that he would hurt or back stab her, but he did.
	
William had fallen for Jessica at this time. Well both Jessica and Pollux. He despised that Jessica had chosen Tom to be hers. William had hate for both Jessica and Pollux and ended up backstabbing Pollux. She thought he actually cared about her. She had hoped so much to have someone like Jessica had Tom. But she found that it was not meant to be.
	
Now not only did William want to capture Jessica and Tom's children, he wanted to capture and own Jessica. The path of Fate started out the day that Jennifer left their home alone and had not noticed that William had planned to set out to capture Jessica. Jennifer looked exactly like Jessica down to the T. Both girls had even got the same tattoos to make sure that they were not set apart. It was a tatic that would prove to set things in motion.
	
While Jennifer was out William had her captured and brought to him. Though he thought that she was actually Jessica. Soon he found that she was actually Jennifer. He was pissed off, but knew things would start to go his way soon. He knew that Jennifer's wife The Cat would come after her and that she did. The Cat left everyone without a word. She went in in blind rage and was captured by William as well.
	
Soon The Mistress would find out what happened. She came to Jessica and Nick to tell them what had happened. Jessica knew it was time to go. Nick being the Caption of the Queen's Guard had to set things in order before he would be able to help. Though not Jessica.
	
Jessica quickly wrote a note on a piece of paper:
	
	"Tom,
	
	I am leaving and taking Ivy with me. You are in charge until I get back. I will be back soon.
	
	Jessica
	-The Queen"
	
Jessica took Ivy with her. There was a good reason for that. Jessica was trying to keep Ivy out of X's hands as he had fallen in love with her and wanted her. Jessica knew that another person, The Nugget was in love with Ivy. Ivy was her Foxxy.
	
So both Jessica and Ivy took off. It was time for the plan to start that Jessica knew of. Jessica and Ivy were to get caught by William and go into the belly of the beast. This is exactly what happened. William would capture them and take them to Gemini Labs to place them into a comma that would send them to the 2nd iteration of Alpha.
```
