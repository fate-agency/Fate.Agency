# Memories
### This is my husband Santa Clause
---
**When you have memories that you can't explain you share them.**
```
   What am I to tell you? 
   I have these memories. 
   They come while I sleep most of the time. 
   Sometimes they come when I am awake too. 
   Anyone can argue that these memory dreams as I call them are not memories at all but fantasies. 
   It is actually fine by me if you do not believe me. 
   All that matters is that I believe it myself. 
   So what happened in this memory. 
   Well let me tell you.

   This memory started out in my house from [Alpha](/docs/alpha). 
   A house I have never seen here on [Earth](/docs/earth). 
   I was in my house seeing family and talking with them. 
   As I am standing there I tell them I have somethings to tell them.

   Mind you that these memories can tend to mix with our lives here on Earth. 
   Well I was trying to introduce this guy to my family from here. 
   It was funny as can be. 
   I say these three things:
```
```
            "First off I have kids."
            "Second I am married."
            "Third my husband is Santa Clause."
```
```
   I then motion at a guy dressed in a Santa Clause outfit. 
   I recognize him as [The Lion](/docs/keys/the-lion). 
   Mind you I don't have kids on Earth nor am I married.
   This was an earlier memory. 
   Before I had a ton of information on any of this.

   My main questions were: 
   How the hell do I know this house and know it is mine? 
   How the hell do I know this man and know him more then anyone in my life? 
   How did I know that he would play Santa for the kids each year? 
   Where were these memories coming from. 
   Could they be false? 
   Of course they could be. 
   Yet they are not false. 
   I hope to prove this to everyone one day that these are actually real memories.
```
