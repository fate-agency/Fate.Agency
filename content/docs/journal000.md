# Journal Entry 
##### 3/18/21

### 14 to 16 weeks?
---
As I woke up this morning I had the thought come into my head. "14 to 16 weeks" I questioned until what? At 14 weeks it will be June 24th 2021. At 16 weeks it will be July 8th 2021. I am completely unsure what these dates mean. Though I feel as if they are important in one way or another. I am unsure if anything will happen on these dates or between these dates, but if something does happen be prepared for anything.

I do feel that something big is coming and its coming faster and faster each and everyday. I am unsure as I said what is coming, but I am hoping it means us being able to finally return to Alpha. If so that will be the best gift ever. Though keep an eye out for anything on these dates. Take photos of strange things, record things as well. Get proof if you can of anything that is not looking or sounding right.

It is about time and soon we will see all.
