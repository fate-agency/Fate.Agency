# Places
## Alpha

### The Capitol
---
The Capitol is the where the President resides. It is unknown at this time who the President is. It is unknown if William, [The Smoker](/docs/keys/the-smoker) is the President now or if he is just pulling the strings behind the scenes. We believe it is one or the other. There is a building within The Capitol that is [The Facility](/docs/the-facility). It is the center of the city. It is the pride of The Capitol.
