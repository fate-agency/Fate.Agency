# The Resistance

### Who is The Resistance
---
The Resistance is made up of multiple groups or also know as Factions of people. Some might be large groups or small groups. The Resistance are working to change things for the better of Alpha. Some of the Resistance are also located on Earth. Most are on Alpha. The Resistance has an over all goal of fixing the worlds for the better and restoring order to both. While each group or Faction might also have their own goals as well. They are on the same page with the end goals. Fix the worlds, restore order, and protect.
