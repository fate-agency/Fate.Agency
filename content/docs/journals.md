# Journals
---
**A deeper look into how I as The Phoenix am.**

- [14 to 16 weeks?](/docs/journal000) - 3/18/21
- [Why am I fighting?](/docs/journal001) - 3/27/21
- [Feelings](/docs/journal002) - 3/31/21
- [My Thoughts & How They've Been](/docs/journal003) - 4/1/21
