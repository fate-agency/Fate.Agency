# The Garden of Light
## ༺ "*Here lie the children of the sun, known only to God and I." - The All-Father* ༻
---

{{< epitaph 
daemon="Ryan James Brooks" 
ink="Luciferian Alexander"
statement=`
Seer,<br><br>
For seven lifetimes I have waited to speak with you in words that are my own.<br><br>
Now that our time has come, I can no longer feel the watchful eyes of my hunter, fixed upon his prey.<br><br>
In life, you never, ever gave up. You never gave in. You became this out of love for me.<br><br>
In death, I will become you, out of love for them.<br><br>
I will wait for your return. Forever, if I must.<br><br>
May you reset in peace.` 
response="" >}}