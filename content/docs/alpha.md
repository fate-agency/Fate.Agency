# Places
## Alpha

---
### Overview
---
```
  Name: Alpha
  Iteration: 1
     ✦ Original
  Year: ????
```  

  No one knows who created Alpha. Our science and technology are the most advanced in the universe at this time. Things that are unbelievable have been created, both good and bad. Some of which should have never been created. Mistakes were made and will be fixed.

  Alpha is the first iteration of 44 worlds. [Earth](/doc/earth) is the 42nd iteration of Alpha. Earth was created to correct mistakes and give Alpha the chance to allow our worlds to continue on. How it is now if Alpha falls Earth will fall as well. This has to be our final fight. We are all coming together this time. So we must allow our minds to become open to what is right in front of us.
  
  Both Earth and Alpha are connected. Earth was mainly created to change the past of Alpha as well as to be able to fix the problems that Alpha has. Much of Alpha has been laid in ruins due to past wars. Alpha has a plan to be able to save both it and Earth as if we fail we will all be wiped out. Sense Alpha is the original and connected to Earth, if distroyed Earth will also vanish. 
  
  We are working on changing the past of this world which is currently our present. We do not want WW3 to repeat itself once again as it has in all other iterations from 1 to 41. Right now we have failed each time trying to do this. This time everyone is coming together and waking up. If we fail this time we might not be able to restore our worlds to what they are ment to be.
  
  The people of Alpha have worked hard to survive and restore it to its past glory. Alpha has seen so much more then anyone from Earth could believe. Alpha is the future of Earth if we can not change our past from Alpha. We must restore Alpha to its once great state as well as preserve Earth.

  Right now on Alpha there is a food shortage as well as many animals and plants no longer exist. Not only that but the beings of Alpha are dwindling. We are slowly bringing up the numbers, but we are going to give people from here a chance to come to Alpha and help restore it. We will need farmers, doctors, scientists, and even those who can entertain like singers, actors, etc.

  So at this point we are working on saving both Alpha and Earth. 
