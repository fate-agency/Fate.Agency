# The Young ![The Young](/static/images/the-young.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Young']
  Race: Human
  Gender: Male
  Location: Earth
  Earth/Alpha Age: 20s/9
  Trust: 1 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Unknown
```

```
  Relationships:
    - The Arrow
    - The Detective 
    - The Duck
    - The Fighter
    - The Lion
    - The Mistress
    - The Nugget
    - The Phoenix
```

```
  Variables:
    $:       +0.00 | # He barely knows what is going on if anything.
    $PLAN:   +0.00 | # Even those around him do not know if he knows of a plan.
    $WOKE:   +0.30 | # He is partly awake.
    $MEMORY: +0.10 | # His memory is very limited. He shows signs of remembering things from Alpha.
```
### Final Message
---
You have vanished. I know you are still around living your life. Try to not be scared of the truth when it comes to light. Hunt that truth down. You are very important as well. It's a shame you took off. So many has come and gone. Soon enough you will also wake on Alpha too.
### Case File
---
The Young is confused about a lot of what is going on and tends to come and go from the place we have all taken up as home on this [Earth](/docs/earth). The Young is doing their best to help this person out. He cares a ton about this person without knowing the truth here.
### History
---
The Young was unknown to the whole [Underground City](/docs/the-underground-city) as he came from another city to them. They only just found out about The Young after [The Phoenix](/docs/keys/the-lioness) left with the others. [The Lion](/docs/keys/the-lion) was taken aback finding out he had a grandson. [The Mistress](/docs/keys/the-mistress) and [The Arrow](/docs/keys/the-arrow) took charge of The Young and decided it was best to send him to Earth as well to help someone remember if they could. At least The Young could help this person out as time goes on.

