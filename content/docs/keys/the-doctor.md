# The Doctor ![The Doctor](/static/images/the-doctor.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['Doc', 'The Dcotor']
  Race: Human
  Gender: Female
  Location: Alpha
  Earth/Alpha Age: ???/???
  Trust: 4 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - $REDACTED
    - Doctor
    - Scientist 
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +1.00 | # She knows what is going on.
    $PLAN:   +0.40 | # We are sure she knows at least part of the plan that is happening.
    $WOKE:   +1.00 | # She is woke.
    $MEMORY: +1.00 | # She has all of her memory.
```
### Case File
---
`$REDACTED`
### History
---
She is one of the few actual medical doctors who is known. She has delivered quite a few babies in her time on [Alpha](/docs/alpha). She works in [The Capitol](/docs/the-capitol) most of the time.
