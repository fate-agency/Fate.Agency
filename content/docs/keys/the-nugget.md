# The Nugget ![The Nugget](/static/images/the-nugget.png)
### RECORD
---
```
  Name: B $REDACTED
  Alias: ['B', 'The Nugget', 'Queen Bitch', 'Princess Bitch']
  Race: Human
  Gender: Female
  Location: Alpha
  Earth/Alpha Age: ???/22
  Trust: 5 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![The Ricks](/static/images/the-ricks-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Random Jobs to Help Others
    - Confidant
```

```	
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Detective 
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
    - The Young
```

```
  Variables:
    $:       +1.00 | # She knows what is going on.
    $PLAN:   +0.40 | # She doesn't know what it is. She only has a clue.
    $WOKE:   +1.00 | # She is woke to what is happening.
    $MEMORY: +1.00 | # She has her complete memory.
```
### Case File
---
  The Nugget is the sister to [The Fighter](/docs/keys/the-fighter). The Nugget and The Fighter are the children of [The Cat](/docs/keys/the-cat) and [The Duck](/docs/keys/the-duck) with a little bit of DNA from [The Dragon](/docs/keys/the-dragon). The Nugget is the niece of [The Lion](/docs/keys/the-lion) and [The Phoenix](/docs/keys/the-lioness). The Nugget is extremely spoiled. She can get her way often and loves her princess and dinosaur shaped chicken nuggets.
### History
---
 The Nugget is extremely important to the awakening of both The Phoenix and The Duck, as is The Fighter. Both keep The Phoenix and The Duck on the right path. They both keep The Phoenix' spirits up as much as they can.

