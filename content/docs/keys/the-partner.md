# The Partner ![The Partner](/static/images/the-partner.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Partner']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: 1 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Mother
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +0.00 | # She does not know.
    $PLAN:   +0.00 | # We are unsure what she knows.
    $WOKE:   +0.00 | # Is not awake.
    $MEMORY: +0.00 | # Does not have any memory at this time.
```
### Final Message
---
We have not met period. I have only heard of you from your partner. You are just as important as the rest of us. Please take care of The Repairer as he needs you more then you know. I am glad you have eachother. Soon enough we will all be home and I can thank you and everyone for all the hard work and effort that has gone into this mission. We are all so damn tired. 
### Case File
---
`$REDACTED`
### History
---
The Partner is the wife of [The Repairer](/docs/keys/the-repairer). She came to [Earth](/docs/earth) to help people out, but does not remember much if anything.
