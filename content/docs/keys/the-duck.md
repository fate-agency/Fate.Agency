# The Duck ![The Duck](/static/images/the-duck.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Duck']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: 35/???
  Trust: 5 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![The Ricks](/static/images/the-ricks-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Medium
    - Mother
    - Seer
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Detective 
    - The Doctor
    - The Dragon
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
    - The Young
```

```
  Variables:
    $:       +1.00 | # She knows what is going on.
    $PLAN:   +0.70 | # She knows there's a plan and knows more then most.
    $WOKE:   +1.00 | # She is awake.
    $MEMORY: +0.20 | # Her memory is limited. She struggles to remember her life on Alpha.
```
### Final Message
---
Soon enough it will be time. Don't abandon me. Please...
### Case File
---
The Duck is now currently on [Earth](/docs/earth) working to help [The Phoenix](/docs/keys/the-lioness) figure out what comes next. It has been a real struggle for both. They are just going with what they know so far. They are more then ready to go home.
### History
---
The Duck is the mother of [The Nugget](/docs/keys/the-nugget) and [The Fighter](/docs/keys/the-fighter), as well as [The Faith](/docs/keys/the-faith) and [The Silent](/docs/keys/the-silent). She was mistaken for The Phoenix when [The Smoker](/docs/keys/the-smoker) came after her. The Duck left out one day alone only to be captured by The Smoker. Once her wife [The Cat](/docs/keys/the-cat) found out, she rushed in without a plan to save The Duck. Well this got The Cat captured as well. [The Mistress](/docs/keys/the-mistress) found out about what happened and gathered The Duck and The Cat's husband, [The Dragon](/docs/keys/the-dragon) and The Phoenix together to inform them of what had happened. As The Dragon was The Captain of The Guard, he had to prepare to go after them on Earth. While this was happening The Phoenix had a vision and prepared herself to go into the belly of the beast with [The Fox](/docs/keys/the-fox). 

