# The Rose
### RECORD
---
```
  Name: Alexandria Maria Pen
  Alias: ['The Rose']
  Race: Human
  Gender: Female
  Location: Alpha
  Earth/Alpha Age: 25/???
  Trust: 5 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Machine](/static/images/the-resistance-badge.png)![Enemies](/static/images/the-enemy-badge.png)
```
  Occupations:
    - Sleeping
```

```
  Relationships:
    - The Phoenix
    - The Rabbit
```

[![Feathergun](/static/images/feathergun.jpg)](/static/images/feathergun.jpg)
