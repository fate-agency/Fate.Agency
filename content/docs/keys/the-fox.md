# The Fox ![The Fox](/static/images/the-fox.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['Fox', 'The Fox']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: 20s/???
  Trust: 3 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Student
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Detective 
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +1.00 | # She knows what is going on.
    $PLAN:   +0.40 | # She knows some of the plan but doubts it.
    $WOKE:   +0.80 | # She is mainly woke, but still does not understand all. 
    $MEMORY: +0.30 | # She has some of her memory, but a lot are false memories that have been implanted.
```
### Final Message
---
No matter if you still believe or not things are happening and you will need to wake up back on Alpha. This world is not our true home and most of us over on Alpha do enjoy our lives with our people. We were not ment to stay here on Earth at this point. We are needing to wake up and go home. One of these days you will see how it is to be. I truely care and never mean to hurt you or anyone. We need this all to get going faster. Soon enough I will make you Gingerbread Cookies over there like I promised you all that time ago.
### Case File
---
The Fox is a cunning thing and is always trying to cause drama. She believes memories that she has to be true when some of them are not. She has yet to remember a lot of her life from Alpha. We believe that her current life on Alpha has been blocked for one reason or another. Or if she remembers then she is not telling us.
### History
---
`$REDACTED`

