# The Arrow ![The Arrow](/static/images/the-arrow.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['Hunter', 'The Arrow']
  Race: Human
  Gender: Male
  Location: Alpha
  Earth/Alpha Age: ???/???
  Trust: 5 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Protector of The Young
```

```
  Relationships:
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
    - The Young
```

```
  Variables:
    $:       +1.00 | # He knows whats going on.
    $PLAN:   +0.70 | # He knows of a plan.
    $WOKE:   +1.00 | # He is awake to what is happening.
    $MEMORY: +1.00 | # He has his complete memory.
```
### Case File
---
Right now The Arrow is to protect and watch over [The Young](docs/keys/the-young). Not much is known about him right now besides working with [The Lion](docs/keys/the-lion) and his team.
### History
---
`$REDACTED`
