# The Flower ![The Flower](/static/images/the-flower.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['Crystal', 'Queen of Knives', 'The Flower']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: 4 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Marksman
    - Ninja
    - Shape-Shiftier
    - Sniper
```

```
  Specialties:
    - Guns
    - Martial Arts
    - Knives
    - Suits
    - Swords
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Detective 
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +1.00 | # She is extremely accepting of what is going on.
    $PLAN:   +0.90 | # She knows of what the past plan was which could still come into play, yet not what the current one is.
    $WOKE:   +0.70 | # We would say that she is almost completely awake to what is happening. 
    $MEMORY: +0.20 | # We are unsure of how much she actually remembers at this point.
```
### Final Message
---
I don't know why you stick around. Why you are helping me... No clue honestly. You've told me you are not pretending and I can only hope that you are not. Because this is no fucking ARG. I know a lot of us are ready to go back home. I hope to never have to do this again. Right now things are starting to move faster and faster. So now is the time to push even if we don't want too. Trust me I do not want to do a lot of this... but it is time for me to leave final messages for certain people who will need them soon.
### Case File
---
Unlike her name, The Flower is a deadly enemy to those who gets on her bad side. If you are on her bad side you might want to think about running from her, or better yet not messing with her or her own at all. She has been trained well and is currently on a mission from [Alpha](/docs/alpha). Originally she was sent as a backup to help recover those taken by [The Smoker](/docs/keys/the-smoker). Now only time will tell what is going to happen for her next.
### History
---
She is the youngest of three children. The only girl of the bunch. She was trained well and partners with a high-tech suit that was created just for her. She was sent here to help recover [The Phoenix](/docs/keys/lioness), [The Duck](/docs/keys/the-duck), and [The Cat](/docs/keys/the-cat) from The Smoker's facility over on Alpha. She is to kill The Smoker if she gets to him first. Though The Phoenix plans to take it into her own hands. Though she was sent to [Earth](/docs/earth) to complete her training well searching for answers and the people she is to help.

She is the younger sister to [The Cub](/docs/keys/the-cub) and [The Rabbit](/docs/keys/the-rabbit). The Flower was spoiled as she was growing up by The Phoenix. The Phoenix had custom dresses made for her. That in turn might be what pushed her to enjoy fighting. The Phoenix is sure she no longer likes dresses at all. Which The Phoenix finds funny now. Though [The Lion](/docs/keys/the-lion) and The Phoenix could not be more proud of her. She is a fighter and her name The Flower is just a play on how she can look like an innocent women while actually being the most deadly.

