# The Cub ![The Cub](/static/images/the-cub.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Cub', 'Johnathan']
  Race: Human
  Gender: Male
  Location: Earth
  Earth/Alpha Age: 30s/25
  Trust: 3 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Coding
    - Father
    - Seer
    - Writer
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Detective
    - The Children
    - The Detective
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +1.00 | # He knows that things are happening.
    $PLAN:   +0.40 | # He knows there is a plan, but does not know what the plan is.
    $WOKE:   +0.20 | # He seems to know more then he is letting on.
    $MEMORY: +0.00 | # He has no memory though he seems to know things that are memories yet he claims they are not.
```
### Final Message
---
Believe or don't we both know the world is changing. I can only hope that things will be fixed. Things are happening and I am so sorry I can not prove these things to you. One day maybe I can truely do so. Hopefully here soon we can guide this world's fate and Alpha's into a bright future. But I fear there is a war in this worlds future no matter if I like it or not.

Alpha is real, and I wish I could prove it to you of all people. Soon enough people will wake up here then there.
### Case File
---
`$REDACTED`
### History
---
The Cub left his family on [Alpha](/docs/alpha) to go and work with [The Mystery](docs/keys/the-mystery) and his team. The Mystery is behind the scenes controlling some of the things that are happening.

