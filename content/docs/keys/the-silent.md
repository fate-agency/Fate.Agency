# The Silent ![The Silent](/static/images/the-silent.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Silent']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: 1 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Unknown
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +0.20 | # She knows about what is partly going on.
    $PLAN:   +0.10 | # We do not know if she knows anything of a plan.
    $WOKE:   +0.00 | # She is not woke.
    $MEMORY: +0.00 | # We are unsure if she remembers anything.
```
### Final Message
---
Sadly we lost touch, but thats ok. You are just one of the few who took off. And seeing as you followed the Dragon as he ran... it makes sense. Just be prepared to wake up soon enough. You will be needed by your family back on Alpha.
### Case File
---
The Silent knows a little bit, but has disappeared from sight.
### History
---
The Silent is the sister of [The Faith](/docs/keys/the-faith). She came in to help find her father [The Dragon](/docs/keys/the-dragon). She was also hoping to help find [The Phoenix](/docs/keys/the-lioness), [The Duck](/docs/keys/the-duck), and [The Cat](/docs/keys/the-cat).
