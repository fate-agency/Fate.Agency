# The Detective ![The Detective](/static/images/the-detective.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Detective']
  Race: Human
  Gender: Male
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: 4 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Coding
    - Decoding
    - Detective
```

```
  Relationships:
    - The Cub
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Lion
    - The Lioness
    - The Nugget
    - The Phoenix
    - The Young
```

```
  Variables:
    $:       +0.20 | # He only knows some of what is going on.
    $PLAN:   +0.00 | # We have no clue if he knows.
    $WOKE:   -1.00 | # He has no clue they are from Alpha.
    $MEMORY: +0.00 | # We have no idea if he has any of their memory.
```
### Case File
---
Right now he is working to uncover a lot of the things that are going on. He only knows some of what is going on that he was told. He was interested in what is really happening. We are unsure how much he might know or if he has any memory at all.
### History
---
We are uncertain of his history on [Alpha](/docs/alpha) right now. We just know that he is from Alpha originally.

