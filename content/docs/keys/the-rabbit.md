# The Rabbit ![The Rabbit](/static/images/the-rabbit.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['Markus', 'Rabbit', 'The Rabbit']
  Race: Human
  Gender: Male
  Location: Alpha
  Earth/Alpha Age: ???/29
  Trust: 4 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Fighter of the Guard
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +1.00 | # He knows that things are happening.
    $PLAN:   +0.00 | # We are unsure if her knows of any plan.
    $WOKE:   +1.00 | # He is woke.
    $MEMORY: +1.00 | # He has his complete memory.
```
### Case File
---
He is safe.
### History
---
The Rabbit is the oldest child of [The Lion](/docs/keys/the-lion) and [The Lioness](/docs/keys/the-lioness). The Rabbit was captured by [The Smoker](/docs/keys/the-smoker) and is now being mind controlled by [The Capitol](/docs/the-capitol) and The Smoker. They try to use The Rabbit as leverage to control those in charge of [The Underground City](/docs/the-underground-city). It never works.

