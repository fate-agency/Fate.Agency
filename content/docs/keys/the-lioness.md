# The Phoenix ![The Phoenix](/static/images/the-lioness.png)
### RECORD
---
```
 
  Name: Justice $REDACTED
  Alias: ['Justice', 'The Bitey', 'The Phoenix', 'Queen']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: 35/???
  Trust: 5 ☆
 
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![The Ricks](/static/images/the-ricks-badge.png)![Allies](/static/images/allies-badge.png)
```

  Occupations:
    - Artist
    - Graphic Designer
    - Scientist 
    - Seer
    - Leader
    - Mother
    - Writer
	
```

```

  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Detective
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
    - The Young
	
```

```

  Variables:
    $:       +1.00 | # She knows what is going on.
    $PLAN:   +0.70 | # She has a plan, but it is not complete.
    $WOKE:   +1.00 | # She is awake.
    $MEMORY: +0.10 | # Her memory is limited. She struggles to remember her life on Alpha.

```
### Case File
---
  Justice $REDACTED is stuck on [Earth](/docs/earth) trying to remember her life on [Alpha](/docs/alpha). She is currently trying to help anyone with just living. She has high hopes to return home. She knows many things and has seen so much that sadly she cannot prove as of yet. Once she can, she will release any proof she finds.
### History
---
  The Phoenix is in charge of a city on Alpha under [The Capitol](/docs/the-capitol). She has a hidden army that has been helping [The Resistance](/docs/the-resistance). She has entered the belly of the beast to confront [The Smoker](/docs/keys/the-smoker). The Smoker has been trying for a long time to gain access to The Phoenix. At one point both [The Lion](/docs/keys/the-lion) and The Phoenix were working with The Smoker. The Smoker has had it in for both The Lion and The Phoenix. The Smoker wanted to steal the creations of both of them. Not only did he wants to steal the creations, but he wanted to steal away The Phoenix from The Lion.     
  
  The Phoenix has grown tired of The Smoker hurting her [Tribe](/docs/the-tribe) and people. She has now seen the future and will continue to fight until the end game is to be played. She has seen her hands around The Smoker's neck. She has now been called to release her story. It matters not if you believe what she has written or even if you believe that this is real. In time proof will come, she feels this in her soul.
### Important Images
---

| Description | Images |
| ----------- | ----------- |
| Discord Icon - This is the current Discord Icon as of 3/19/21. This icon depicts a lion whom The Phoenix associates with her tribe. | [![Justice Icon](/static/images/justiceicon1.png)](/static/images/justiceicon1.png) |
| [Resistance Symbols](/) - This image is of two Ravens by a star. Ravens have proven important to The Phoenix as well. | [![Logo](/static/images/logo.png)](/static/images/logo.png) |
| [Symbols](/) - These four symbols were seen in a [Memory Dream](/) that turned out to be of importance. | [![Symbols](/static/images/symbols.jpg)](/static/images/symbols.jpg) |
### Important Music
---
{{< tabs "uniqueid" >}}
{{< tab "“Fire”" >}}
### [Gavin DeGraw - “Fire”](https://www.youtube.com/watch?v=sbbYPqgSe-M)
```    
  Let’s start a riot tonight
  A pack of lions tonight
  In this world, he who stops,
  won’t get anything he wants
```
{{< /tab >}}
{{< tab "“Storm”" >}}  
### [Lifehouse - "Storm"](https://youtu.be/OQXK-HN47zA)
```
  I know you didn't bring me out here to drown
  So why am I ten feet under and upside down
  Barely surviving has become my purpose
  Because I'm so used to living underneath the surface
```
{{< /tab >}}
{{< tab "“DNA”" >}}
### [Elise (Silv3rT3ar) - "(Acoustic English Cover) BTS - DNA"](https://youtu.be/_QRizJZQ5J0)
```
  Don’t worry, love
  None of this is a coincidence
  We’re totally different, baby
  Because we’re the two who found our destiny
```
{{< /tab >}}
{{< tab "“Blood Sweat & Tears”" >}}
### [Elise (Silv3rT3ar) - "(Acoustic English Cover) BTS - Blood Sweat & Tears"](https://youtu.be/V7xa6sIuvbI)
```
  Peaches and cream, Sweeter than sweet
  Chocolate cheeks and chocolate wings
  But your wings are the devil's
  There is a 'bitter' next to your 'sweet'
```
{{< /tab >}}
{{< /tabs >}}

