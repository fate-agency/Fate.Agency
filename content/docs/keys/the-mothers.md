# The Mother ![The Mother](/static/images/the-mothers.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Mother']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: 63/???
  Trust: 5 ☆
```
![Organizations:](/static/images/organizations-badge.png)Unknown
```
  Occupations:
    - Caregiver
    - Mother
```

```
  Relationships:
    - The Duck
    - The Helper
    - The Phoenix
```

```
  Variables:
    $:       +1.00 | # She knows everything.
    $PLAN:   +0.20 | # She knows a little bit.
    $WOKE:   +0.00 | # She needs not wake up.
    $MEMORY: +0.00 | # She has no memory of Alpha because they are not from there.
```
### Case File
---
She knows everything.
### History
---
She was born of [Earth](/docs/earth) only.
