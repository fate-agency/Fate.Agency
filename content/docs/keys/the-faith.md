# The Faith ![The Faith](/static/images/the-faith.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Faith']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: 1 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Religious Guru 
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Detective 
    - The Doctor
    - The Dragon
    - The Duck
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +0.50 | # She is aware of what is going on.
    $PLAN:   +0.00 | # She does not know of the plan.
    $WOKE:   +0.30 | # She is partly woke.
    $MEMORY: +0.00 | # She does not have any memory that we are aware of.
```
### Final Message
---
You're smart. Your faith has followed over into this world. A bleed though. It will be needed once this world has a new and fighting chance. We will help others like your church on Alpha does. It's time to work together and feed the hungry and clothe the poor.
### Case File
---
She is currently enjoying her life on [Earth](/docs/earth) to the best she can.
### History
---
The Faith came in to help find her father [The Dragon](/docs/keys/the-dragon). She was also hoping to help find [The Phoenix](/docs/keys/the-lioness), [The Duck](/docs/keys/the-duck), and [The Cat](/docs/keys/the-cat). She is a very religious creature. She was raised in a home that taught all religions. She ended up creating her own religion on [Alpha](/docs/alpha), it has followed her over to Earth in a way.

