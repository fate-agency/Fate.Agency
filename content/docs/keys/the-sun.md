# The Sun ![The Sun](/static/images/the-sun.png)

### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Sun']
  Race: Human
  Gender: Male
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: 1 ☆
```
![Organizations:](/static/images/organizations-badge.png)Unknown
```
  Occupations:
    - Unknown
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Children
    - The Cub
    - The Detective 
    - The Dragon
    - The Faith
    - The Flower
    - The Fox
    - The Mistress
    - The Phoenix
    - The Repairer
    - The Silent
    - The Young
```

```
  Variables:
    $:       +0.00 | # He does not know what is going on.
    $PLAN:   +0.00 | # We do not know if he knows.
    $WOKE:   +0.00 | # He shows no signs.
    $MEMORY: +0.00 | # We are unsure if he is from Alpha.
```
### Case File
---
`REDACTED`
### History
---
So far we know very little of The Sun. We just know that he might be involved somehow. Though he might not know it.
