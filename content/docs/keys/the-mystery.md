# The Mystery ![The Mystery](/static/images/the-mystery.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Mystery', 'X']
  Race: Human
  Gender: Male
  Location: Alpha
  Earth/Alpha Age: ???/???
  Trust: 4 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Leader of another Faction
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +1.00 | # He knows and understands what is going on.
    $PLAN:   +0.80 | # He has his own plan, but it helping The Lion and The Phoenix.
    $WOKE:   +1.00 | # He is completely woke.
    $MEMORY: +1.00 | # He has his full memory.
```
### Case File
---
The Mystery is now working as his own leader of a team of people. He is slowly helping change history to fit what is needed. Though not as much as [The Smoker](/docs/keys/the-smoker) is trying to change.
### History
---
The Mystery is in charge of his own Faction or team. He has some of the same goals as [The Lion](/docs/keys/the-lion) and [The Phoenix](/docs/keys/the-lioness). Of course The Mystery is also in it for himself. As [The Capitol](/docs/the-capitol) has things that he wants, just like The Lion and The Phoenix has things they want from The Capitol.
