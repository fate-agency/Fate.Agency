# The Witch ![The Witch](/static/images/the-witch.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Witch']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: -2 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Enemy](/static/images/the-enemy-badge.png)
```
  Occupations:
    - Mother
    - Seer
    - Trickster
    - Witch
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
```

```
  Variables:
    $:       +0.10 | # She knows some things.
    $PLAN:   +0.00 | # She knows of no plan.
    $WOKE:   +0.05 | # She is not awake.
    $MEMORY: +0.10 | # Her memory is extremely limited.
```
### Final Message
---
There is always a chance to change your ways. It does not mean I trust you one bit. You've got things to answer for back on Alpha as well as here. But live what life you have. Enjoy it while you can, as things are coming and you know it. You've basicly gotten in the little black book in my head. So wake up and stop being a cunt at times. Don't contact me.
### Case File
---
`$REDACTED`
### History
---
[The Capitol](/docs/the-capitol) took The Witch and [The Bewitched's](/docs/keys/the-bewitched) children as a ransom so that they would help them take control of all the cities as well as to trick [The Lion](/docs/keys/the-lion) and [The Phoenix](/docs/keys/the-lioness). The Capitol has promised that they would have a life here on [Earth](/docs/earth) with their children that would be better then on [Alpha](/docs/alpha) as long as they would work for them. They had no choice as if they did not agree The Capitol would kill their three children.

