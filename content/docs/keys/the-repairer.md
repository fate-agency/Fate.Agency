# The Repairer ![The Repairer](/static/images/the-repairer.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Repairer', 'Uncle']
  Race: Human
  Gender: Male
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: 5 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![The Ricks](/static/images/the-ricks-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Father
    - Mechanics
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +0.00 | # He has a great understanding to what is going on.
    $PLAN:   +0.30 | # We are unsure how much he knows.
    $WOKE:   +0.80 | # He is mostly awake to what is going on. 
    $MEMORY: +0.10 | # He remembers some of Alpha, but not a lot.
```
### Final Message
---
You've been more important then you could know. Trust me I am around thanks to you. Things got extremely dark last year and you were there to help. I am sorry I have been slacking on my end with chating with you. Soon enough we will be free to go back home. Soon enough. Thank you.
### Case File
---
He is currently on earth and knows a bit of what is going on.
### History
---
The Repairer is the best friend of [The Phoenix](/docs/keys/the-lioness). They are also the husband of [The Partner](/docs/keys/the-partner). The Repairer tends to work on mechanical things. He enjoys working on and repairing vehicles.

