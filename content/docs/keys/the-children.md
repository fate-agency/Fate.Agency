# The Children ![The Children](/static/images/the-children.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Children']
  Race: Human
  Gender: ???
  Location: Earth/Alpha
  Earth/Alpha Age: ???/???
  Trust: 0 ☆
```
![Organizations:](/static/images/organizations-badge.png) Unknown
```
  Occupations:
    - Unknown
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +0.00 | # They have no idea what is happening.
    $PLAN:   +0.00 | # They have no clue.
    $WOKE:   +0.00 | # They are not awake.
    $MEMORY: +0.00 | # They do not remember.
```
### Case File
---
At this point we know of one case that children are currently being used as blackmail here on [Earth](/docs/earth) from [Alpha](/docs/alpha). [The Capitol](/docs/the-capitol) loves to bribe others to fight for them by kidnapping their children. Not only that they will make it seem as if it is the parents best interest to take any offer that'll help them. Such as being permitted to start a new life on a new planet with their children.
### History
---
The Children have been captured to be used as leverage to control their parents. A clear example is [The Witch](/docs/keys/the-witch) and [The Bewitched's](/docs/keys/the-bewitched) children. Though we do not doubt that this is a tactic often used by The Capitol. The Capitol has been known to use children to further their progress. They will use them to fight in wars that the children should not have to fight in at such a young age.

