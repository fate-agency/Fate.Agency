# The Dragon ![The Dragon](/static/images/the-dragon.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['Nick', 'The Dragon']
  Race: Human
  Gender: Male
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: 1 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Captain of the Guard
    - Father
    - Mechanics
    - Robotics
    - Trickster
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Detective
    - The Doctor
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +0.30 | # He knows a bit of whats happening. He just doesn't believe.
    $PLAN:   +0.00 | # He has no clue.
    $WOKE:   +0.10 | # He just doesn't want to believe.
    $MEMORY: +0.10 | # His memory is extremely limited, though sometimes it shines through.
```
### Final Message
---
Nick you must wake up. You won't see this more then likely until it is too late. There is the chance that you will see this before then, but if we have returned before you, you will need to find a way to wake up. Not death. But remember back to saying "In THIS body I think I am called _____" as a focus point. Why in the ever loving hell would that be your thought on this world if nothing is up?

We will be waiting for you Nick. You will be needed back home.
### Case File
---
The Dragon plays his part as a trickster very well. He is in denial about any of this. Yet he has memories from [Alpha](/docs/alpha) that he just believes to be not true. He needs to wake up to what is going on. Though it could take an extremely long time before this happens. It might not happen until he wakes up back on Alpha.
### History
---
The Dragon is the Captain of the Guard for [The Underground City](/docs/the-underground-city). He had to get things in order before he could come here to save his wives, [The Duck](/docs/keys/the-duck) and [The Cat](/docs/keys/the-cat). He could not leave them stuck here. He then came to Earth as soon as he could. He got distracted, putting it a nice way, to where he was unable to complete his mission.

