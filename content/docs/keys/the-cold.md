# The Cold ![The Cold](/static/images/the-cold.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Cold']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: 1 ☆
```
![Organizations:](/static/images/organizations-badge.png)Unknown
```
  Occupations:
    - Unknown
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Children
    - The Cub
    - The Detective 
    - The Dragon
    - The Faith
    - The Flower
    - The Fox
    - The Mistress
    - The Phoenix
    - The Repairer
    - The Silent
    - The Young
```

```
  Variables:
    $:       +0.00 | # She does not know what is going on.
    $PLAN:   +0.00 | # We do not know if she knows.
    $WOKE:   +0.00 | # She shows no signs.
    $MEMORY: +0.00 | # We are unsure if she is from Alpha.
```
### Case File
---
`REDACTED`
### History
---
So far we know very little of The Cold. We just know that she might be involved somehow. Though she might not know it.
