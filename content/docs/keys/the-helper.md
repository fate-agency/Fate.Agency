# The Helper ![The Helper](/static/images/the-helper.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['The Helper']
  Race: Human
  Gender: Male
  Location: Earth
  Earth/Alpha Age: 16/16
  Trust: 1 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Student
    - Child
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +1.00 | # He knows what is going on but is doubtful.
    $PLAN:   +0.70 | # He knows the overall plan, but doesn't seem to care yet.
    $WOKE:   +0.00 | # He is not woke.
    $MEMORY: +0.00 | # He doesn't remember anything, but is open to knowing.
```
### Final Message
---
I love you kiddo. If I do vanish I will see you later on. I hope things go good here soon. You know how much I love you. You're like my own child. Be good and be ready to protect. Thank you for loving and caring for me.
### Case File
---
`$REDACTED`
### History
---
He is an orphan on [Alpha](/docs/alpha). He is from [The Underground City](/docs/the-underground-city).
