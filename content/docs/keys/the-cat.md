# The Cat ![The Cat](/static/images/the-cat.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['Cat', 'The Cat']
  Race: Human
  Gender: Female
  Location: Earth
  Earth/Alpha Age: 21/???
  Trust: 4 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![The Ricks](/static/images/the-ricks-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Unknown
    - Mother
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +0.50 | # She knows quite a lot more then she lets on.
    $PLAN:   +0.00 | # She does not know of any plan.
    $WOKE:   +0.20 | # She is fighting to not wake up in fear of remembering to much.
    $MEMORY: +0.30 | # We are unsure, but believe she remembers more then she lets on.
```
### Final Message
---
I hope things continue to get better for you. I will be waiting on you. I have hope still.
### Case File
---
$REDACTED
### History
---
The Cat ran in after [The Duck](/docs/keys/the-duck) without thinking after finding out that The Duck was captured. Thus she got herself captured as well. She is the Wife of The Duck and [The Dragon](/docs/keys/the-dragon). She is the mother of [The Nugget](/docs/keys/the-nugget) and The [Fighter](/docs/keys/the-fighter).
