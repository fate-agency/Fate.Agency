# The Bewitched ![The Bewitched](/static/images/the-bewitched.png)
### RECORD
---
```
  Name: $REDACTED
  Alias: ['Adian', 'The Bewitched']
  Race: Human
  Gender: Male
  Location: Earth
  Earth/Alpha Age: ???/???
  Trust: -1 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Enemy](/static/images/the-enemy-badge.png)
```
  Occupations:
    - Father
    - Helper of the Witch
```

```
  Relationships:
    - The Arrow
    - The Cat
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
```

```
  Variables:
    $:       +0.10 | # He knows some things.
    $PLAN:   +0.00 | # He knows of no plan.
    $WOKE:   +0.05 | # He is not awake.
    $MEMORY: +0.10 | # His memory is extremely limited.
```
### Final Message
---
Wake up brother. Stop being tricked and used.
### Case File
---
`$REDACTED`
### History
---
[The Capitol](/docs/the-capitol) took The Bewitched and [The Witch's](/docs/keys/the-witch) children as a ransom so that they would help them take control of all the cities as well as to tick [The Lion](/docs/keys/the-lion) and [The Phoenix](/docs/keys/the-lioness). The Capitol has promised that they would have a life here on Earth with their children that would be better then on [Alpha](/docs/alpha) as long as they would work for them. They had no choice as if they did not agree The Capitol would kill their three children.

