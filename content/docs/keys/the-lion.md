# The Lion ![The Lion](/static/images/the-lion.png)
### RECORD
---
```
  Name: Tom $REDACTED
  Alias: ['Boss', 'God', 'Thomas', 'Thoumas', 'Tom']
  Race: Human
  Gender: Male
  Location: Alpha
  Earth/Alpha Age: ???/???
  Trust: 5 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![The Ricks](/static/images/the-ricks-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Father
    - Scientist 
    - Leader
```

``` 
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Detective 
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Fighter
    - The Flower
    - The Fox
    - The Helper
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
    - The Young
```

```
  Variables:
    $:       +1.00 | # He knows what is going on.
    $PLAN:   +0.70 | # He has a plan, but it is not complete.
    $WOKE:   +1.00 | # He is awake and working on fixing things. 
    $MEMORY: +1.00 | # He has his complete memory.
```	
### Case File
---
  Tom $REDACTED is currently running [The Underground City](/docs/the-underground-city) on [Alpha](/docs/alpha). He is heading the fight to get those who are on Earth back home to Alpha. He has been working with a team to gain the knowledge on how to return [The Phoenix](/docs/keys/the-lioness), [The Duck](/docs/keys/the-duck), and [The Cat](/docs/keys/the-cat) back to their rightful world. He is currently working on that project along with other side projects.
### History
---
  The Lion was left in charge of The Underground City on Alpha when The Phoenix had to leave unexpectedly. The Lion is now fighting to return [The Tribe](/docs/the-tribe) and others to Alpha.

