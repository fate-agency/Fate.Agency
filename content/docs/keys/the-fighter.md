# The Fighter ![The Fighter](/static/images/the-fighter.png)
### RECORD
---
```
  Name: J $REDACTED
  Alias: ['J', 'The Fighter']
  Race: Human
  Gender: Male
  Location: Alpha
  Earth/Alpha Age: ???/22
  Trust: 5 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Resistance](/static/images/the-resistance-badge.png)![The Ricks](/static/images/the-ricks-badge.png)![Allies](/static/images/allies-badge.png)
```
  Occupations:
    - Random Jobs to Help Others
    - Confidant
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Detective 
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Nugget
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Smoker
    - The Witch
    - The Young
```

```
  Variables:
    $:       +1.00 | # He knows what is going on.
    $PLAN:   +0.40 | # He doesn't know what it is. He only has a clue.
    $WOKE:   +1.00 | # He is woke to what is happening.
    $MEMORY: +1.00 | # He has her complete memory.
```
### Case File
---
`$REDACTED`
### History
---
The Fighter is the brother of [The Nugget](/docs/keys/the-nugget) and the Son of [The Cat](/docs/keys/the-cat), [The Duck](/docs/keys/the-duck), and [The Dragon](/docs/keys/the-dragon).

