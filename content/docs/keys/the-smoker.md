# The Smoker ![The Smoker](/static/images/the-smoker.png)
### RECORD
---
```
  Name: William $REDACTED
  Alias: ['Bill', 'Billy Boy', 'Chain Smoker', 'Jerry', 'The Smoker', 'Will', 'William']
  Race: Human
  Gender: Male
  Location: Alpha
  Earth/Alpha Age: ???/???
  Trust: -5 ☆
```
![Organizations:](/static/images/organizations-badge.png)![The Enemy](/static/images/the-enemy-badge.png)
```
  Occupations:
    - Head Researcher for the Capitol
    - Scientist
```

```
  Relationships:
    - The Arrow
    - The Bewitched
    - The Cat
    - The Children
    - The Cub
    - The Doctor
    - The Dragon
    - The Duck
    - The Faith
    - The Flower
    - The Fox
    - The Helper
    - The Lion
    - The Mistress
    - The Mystery
    - The Partner
    - The Phoenix
    - The Rabbit
    - The Repairer
    - The Silent
    - The Witch
```

```
  Variables:
    $:       +1.00 | # He knows what is going on.
    $PLAN:   -0.20 | # He might have a clue what is happening, but if so its minor.
    $WOKE:   +1.00 | # He is awake to what is happening of course.
    $MEMORY: +1.00 | # He has his full memory.
```
### Final Message
---
William. I am coming for you. You will not win and you will not keep us here. You've lost our bodies there on Alpha and it is only a matter of time now. The Federation is coming for us and to put your final moments to the test. You might think about running, but trust me they will find you anywhere. They can and will. It's always a matter of time. If need be I will find you once I am fully restored to who I once was. You thought I was bad then, just wait until I see you again. War was declared a long while ago between us. You no longer have those I love under your control there. We are coming for you. I will be there when you are judged. 

It is time to choose fight, fawn, or flight. I figured you will fight over fawn any day, but I would not put it past you to fly once you see for sure that this "princess" is not a princess and will be coming to wipe you from Alpha and Earth. If I am wrong for wanting this, then so be it. But knowing what horrors you have done to my people, family, and I... you will be paid back. Just be prepared. My hands could still end up around your neck with little to no care if you die or not. The final blow will be for the Federation to decide. You deserve justice and not mercy. I would normally show mercy, but you have been given that way to many times in your long long life. I will not waver this time Billy Boy.

{{< tabs "uniqueid" >}}
{{< tab "“Outrunning Karma”" >}}
### [Alec Benjamin - "Outrunning Karma"](https://www.youtube.com/watch?v=e4vD2S5vQMM)
```
Outrunning karma, that boy
He's such a charmer, all the
Bugs and their larva follow
Him out to Colorado
Ten-dozen hearts in a bag
Their bodies lyin', he'll drag
Them down to Colorado
A modern desperado

And he'll race for miles through the night
He runs because he knows he cannot hide

He's never gonna make it, all the
Poor people he's forsaken, karma
Is always gonna chase him for his lies
It's just a game of waiting from the
Church steeple down to Satan karma
There's really no escape until he dies

Outrunning karma, that boy
Can't run no farther, it's the
Last days of Sparta, follow
Him down to meet Apollo

And he'll brace for battle in the night
He'll fight because he knows he cannot hide

He's never gonna make it, all the
Poor people he's forsaken, karma
Is always gonna chase him for his lies
It's just a game of waiting from the
Church steeple down to Satan karma
There's really no escape until he dies

La-la-la, la la, la-la-la, la la
La-la-la, la la, la la
La-la-la, la la, la-la-la, la la
La-la-la, la la, la la

He's never gonna make it, all the
Poor people he's forsaken, karma
Is always gonna chase him for his lies
It's just a game of waiting from the
Church steeple down to Satan karma
There's really no escape until he dies

La-la-la, la la, la-la-la, la la
La-la-la, la la, la la
```
{{< /tab >}}
{{< /tabs >}}

### Case File
---
William $REDACTED is currently holding the bodies of those who have resisted in a building in [The Capitol](/docs/the-capitol). He aimed to capture [The Phoenix](/docs/keys/the-lioness) originally, but instead he captured [The Duck](/docs/keys/the-duck) instead. So instead of gaining the one he wanted he gained both The Duck and [The Cat](/docs/keys/the_cat). Once The Phoenix found out she and [The Fox](/docs/keys/the-fox) then intentionally got captured to go into the belly of the beast. Now William is working to keep all of them there and asleep living their lives on [Earth](/docs/earth) in a whole different body.
### History
---
William $REDACTED has worked with both [The Lion](/docs/keys/the-lion) and The Phoenix in the past. He holds a vendetta against them both. He right now is thought to either be in control of The Capitol by either being the President of The Capitol or he is pulling the strings of the President. Either way this makes him a danger to every single person on [Alpha](/docs/alpha).

