# Places
## Earth

---
### Overview
---
```
  Name: Earth
  Iteration: 42
     ✦ Of Alpha
  Year: 2021
```

  Earth is a younger copy of [Alpha](/docs/alpha). We here on Earth are reliving the history of Alpha. Though things happening on Alpha are causing changes here on Earth which in turn are causing changes on Alpha. 
  
  One of the largest things to happen in 2020 was the USA election. The president of the USA was to still be Trump and he was going to take control and turn the USA into a communist country. Though the people from Alpha, in [The Capitol](/docs/the-capitol), had decided to cause it to were Biden would win. That changed the future on Alpha. Now the USA on Alpha is no longer communist. Though we now know why they wanted that change. [The Smoker](/docs/keys/the-smoker), William, had planned this so that he could gain control of The Capitol on Alpha. Right now we suspect that William is either now the President of the Capitol or at least pulling the strings of the leader.
